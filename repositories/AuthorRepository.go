package repositories

import (
	"context"

	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type AuthorRepository struct {
	db *mongo.Database
}

type CreateAuthorRequest struct {
	Name        string
	Description string
}

func (repo *AuthorRepository) Create(request CreateAuthorRequest) (string, error) {
	res, err := repo.db.Collection("author").InsertOne(context.TODO(), request)
	if err != nil {
		return "", err
	}
	objectId, _ := res.InsertedID.(primitive.ObjectID)
	return objectId.Hex(), nil
	// if err != nil {
	// 	return book
	// }
}

func NewAuthorRepository(db *mongo.Database) *AuthorRepository {
	return &AuthorRepository{db}
}
