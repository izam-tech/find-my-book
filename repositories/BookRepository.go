package repositories

import (
	"context"

	"bitbucket.org/izam-tech/find-my-book/models"
	"go.mongodb.org/mongo-driver/mongo"
)

type BookRepository struct {
	db *mongo.Database
}

func (BR *BookRepository) create(book *models.Book) *models.Book {
	_, err := BR.db.Collection("books").InsertOne(context.TODO(), book)
	if err != nil {
		return book
	}
	return book

}

// func (BR *BookRepository) all() []models.Book {
// 	cur, err := BR.db.collection("books").Find(context.TODO(), bson.M{})
// 	if err != nil {
// 		config.GetError(err, w)
// 		return
// 	}
// 	var books []models.Book
// 	// Close the cursor once finished
// 	/*A defer statement defers the execution of a function until the surrounding function returns.
// 	simply, run cur.Close() process but after cur.Next() finished.*/
// 	defer cur.Close(context.TODO())

// 	for cur.Next(context.TODO()) {

// 		// create a value into which the single document can be decoded
// 		var book models.Book
// 		// & character returns the memory address of the following variable.
// 		err := cur.Decode(&book) // decode similar to deserialize process.
// 		if err != nil {
// 			log.Fatal(err)
// 		}

// 		// add item our array
// 		books = append(books, book)
// 	}

// 	if err := cur.Err(); err != nil {
// 		log.Fatal(err)
// 	}

// 	return books

// }

func NewBookRepository(db *mongo.Database) *BookRepository {
	return &BookRepository{db}
}
