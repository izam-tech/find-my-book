//+build wireinject

package main

import (
	"bitbucket.org/izam-tech/find-my-book/cmd"
	"bitbucket.org/izam-tech/find-my-book/config"
	"bitbucket.org/izam-tech/find-my-book/controllers"
	"bitbucket.org/izam-tech/find-my-book/repositories"
	"bitbucket.org/izam-tech/find-my-book/services"
	"github.com/google/wire"
)

func CreateBookContrller() *controllers.BookController {
	panic(wire.Build(
		config.NewConfig,
		config.ConnectDB,
		repositories.NewBookRepository,
		services.NewBookService,
		controllers.NewBookController,
	))
}

func CreateAuthorController() *controllers.AuthorController {
	panic(wire.Build(
		config.NewConfig,
		config.ConnectDB,
		repositories.NewAuthorRepository,
		services.NewAuthorService,
		controllers.NewAuthorController,
	))
}

func CreateServer() *cmd.Server {
	panic(wire.Build(
		config.NewConfig,
		CreateBookContrller,
		CreateAuthorController,
		cmd.New,
	))
}
