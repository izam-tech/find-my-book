package models

import "time"

type Author struct {
	_id         string
	name        string
	description string
}

type Publisher struct {
	_id         string
	name        string
	description string
}

type Category struct {
	_id         string
	name        string
	description string
}

type User struct {
	_id      string
	username string
	email    string
	password string
	photo    string
}

type Review struct {
	_id     int
	text    string
	user_id int
}

type Book struct {
	_id          int
	title        string
	publisher_id int
	author_id    int
	reviews      []Review
	DateJoined   time.Time
}
