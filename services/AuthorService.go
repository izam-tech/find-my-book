package services

import "bitbucket.org/izam-tech/find-my-book/repositories"

type AuthorService struct {
	repo *repositories.AuthorRepository
}

func (service *AuthorService) Add(name string, description string) (string, error) {
	return service.repo.Create(repositories.CreateAuthorRequest{name, description})
}

func NewAuthorService(repo *repositories.AuthorRepository) *AuthorService {
	return &AuthorService{repo}
}
