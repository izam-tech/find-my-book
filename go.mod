module bitbucket.org/izam-tech/find-my-book

go 1.13

require (
	github.com/godoctor/godoctor v0.0.0-20181123222458-69df17f3a6f6 // indirect
	github.com/google/wire v0.4.0
	github.com/gorilla/mux v1.7.4
	go.mongodb.org/mongo-driver v1.3.1
	labix.org/v2/mgo v0.0.0-20140701140051-000000000287
	launchpad.net/gocheck v0.0.0-20140225173054-000000000087 // indirect
)
