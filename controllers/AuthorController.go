package controllers

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/izam-tech/find-my-book/services"
)

type AuthorController struct {
	AuthorService *services.AuthorService
}

type requestError struct {
	message string
	code    int
	errors  []string
}

type AppResponse struct {
	Message string
	Code    int
	Data    map[string]string
}

func (controller *AuthorController) Create(w http.ResponseWriter, r *http.Request) {
	name := r.FormValue("name")
	var code int
	var message string
	description := r.FormValue("description")
	var appResponse AppResponse
	responseData := make(map[string]string)
	if name == "" {
		code = 400
		message = "invalid datasubmitted"
		responseData["name"] = "required"
	} else {
		res, err := controller.AuthorService.Add(name, description)
		if err != nil {
			code = 500
			message = "internal server error"
		} else {
			code = 201
			message = "author saved successfully"
			responseData["id"] = res
		}
	}
	appResponse = AppResponse{message, code, responseData}
	response, err := json.Marshal(appResponse)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(response)
	return
}

func NewAuthorController(service *services.AuthorService) *AuthorController {
	return &AuthorController{service}
}
