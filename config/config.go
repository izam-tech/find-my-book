package config

type Config struct {
	dbName     string
	dbHost     string
	dbPassword string
	dbPort     string
	Host       string
	Port       string
}

func NewConfig() *Config {
	return &Config{
		"find-my-book",
		"mongodb://127.0.0.1",
		"",
		"27017",
		"127.0.0.1",
		"80",
	}
}
