package cmd

import (
	"log"
	"net/http"

	"bitbucket.org/izam-tech/find-my-book/config"
	"bitbucket.org/izam-tech/find-my-book/controllers"
	"github.com/gorilla/mux"
)

type Server struct {
	config           *config.Config
	bookController   *controllers.BookController
	authorController *controllers.AuthorController
}

func New(
	config *config.Config,
	bookController *controllers.BookController,
	authorController *controllers.AuthorController,
) *Server {
	return &Server{
		config,
		bookController,
		authorController,
	}
}

func (s *Server) Handler() http.Handler {
	r := mux.NewRouter()
	r.HandleFunc("/authors", s.authorController.Create).
		Methods("POST")
	return r
}

func (s *Server) Run() {
	httpServer := &http.Server{
		Addr:    s.config.Host + ":" + s.config.Port,
		Handler: s.Handler(),
	}
	log.Fatal(httpServer.ListenAndServe())

}
